package com.example.akash.leakcannaryexplored;

import android.app.Application;
import android.content.Context;

import com.squareup.leakcanary.LeakCanary;
import com.squareup.leakcanary.RefWatcher;


public class AppController extends Application {
    private RefWatcher objectWatcher;

    public static RefWatcher getRefWatcher(Context context) {
        AppController applicationContext = (AppController) context.getApplicationContext();
        return applicationContext.objectWatcher;
    }


    @Override
    public void onCreate() {
        super.onCreate();
        objectWatcher = LeakCanary.install(this);
    }

}
