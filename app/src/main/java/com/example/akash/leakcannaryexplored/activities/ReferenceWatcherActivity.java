package com.example.akash.leakcannaryexplored.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.ImageView;

import com.example.akash.leakcannaryexplored.AppController;
import com.example.akash.leakcannaryexplored.R;
import com.squareup.leakcanary.RefWatcher;

public class ReferenceWatcherActivity extends AppCompatActivity {
    private static ImageView mImageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_activity_reference_watcher);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        RefWatcher refWatcher = AppController.getRefWatcher(getApplicationContext());
        mImageView = (ImageView) findViewById(R.id.imageView);
        refWatcher.watch(mImageView);

    }

}
