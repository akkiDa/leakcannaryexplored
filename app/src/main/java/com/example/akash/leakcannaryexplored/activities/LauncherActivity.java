package com.example.akash.leakcannaryexplored.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.example.akash.leakcannaryexplored.R;

public class LauncherActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_activity_launcher);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setListners();
    }

    private void setListners() {
        findViewById(R.id.bt_handler_leak).setOnClickListener(this);
        findViewById(R.id.bt_imageview_leak).setOnClickListener(this);
    }

    public void startleakActivity() {
        Intent intent = new Intent(this, HandlerLeakActivity.class);
        startActivity(intent);
    }

    public void startReferenceWatcherActivity() {
        Intent intent = new Intent(this, ReferenceWatcherActivity.class);
        startActivity(intent);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.bt_handler_leak:
                startleakActivity();
                break;
            case R.id.bt_imageview_leak:
                startReferenceWatcherActivity();
                break;

        }
    }
}
